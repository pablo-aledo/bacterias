all:
	make -C src
	
clean:
	rm -rf 0 1 model output.png
	mkdir 0 1 
	rm -f Error/output.png
	#killall train
	#killall clasify
	killall make

process: 
	#export PATH=$PWD/bin/:$PATH
	#split Originales/train1.png Originales/train2.png 20 0
	#split Originales/train1.png Originales/train2.png 20 5
	#split Originales/train1.png Originales/train2.png 20 10
	#split Originales/train1.png Originales/train2.png 20 15	
	#split Originales/train1.png Originales/train2.png 20 20
	#split Originales/train1.png Originales/train2.png 20 25
	#split Originales/train1.png Originales/train2.png 20 30
	#split Originales/train1.png Originales/train2.png 20 35
	#split Originales/train1.png Originales/train2.png 20 40
	#split Originales/train1.png Originales/train2.png 10 90
	ls 0 | wc -l
	ls 1 | wc -l
	train
	clasify Originales/input.png Originales/clasif.png 20
	#clasify Originales/train1.png 10
	sudo cp output.png ../Error
	

prueba:
	for number in 1 2 3 4 5 6 7 8 9 10; do \
		\cp `find ../BD1/Resplit/1/*.png | head -n $$((35*$$number)) | tail -n 35` 1/ ; \
		\cp `find ../BD1/Resplit/0/*.png | head -n $$((35*$$number)) | tail -n 35` 0/ ; \
		\cp `find ../BD1/Split/1/*.png   | head -n $$((15*$$number)) | tail -n 15` 1/ ; \
		\cp `find ../BD1/Split/0/*.png   | head -n $$((15*$$number)) | tail -n 15` 0/ ; \
		ls 0 | wc -l ; \
		ls 1 | wc -l ; \
		train ; \
		clasify Originales/input.png Originales/clasif.png 20; \
		\cp output.png Error/output.png ; \
		\cp output.png Error/Train1/output-$$number.png ; \
		estadistica Error/output.png Originales/clasif.png ; \
	done
	for number in 20 30 40 50; do \
		\cp `find ../BD1/Resplit/1/*.png | head -n $$((35*$$number)) | tail -n 350` 1/ ; \
		\cp `find ../BD1/Resplit/0/*.png | head -n $$((35*$$number)) | tail -n 350` 0/ ; \
		\cp `find ../BD1/Split/1/*.png   | head -n $$((15*$$number)) | tail -n 150` 1/ ; \
		\cp `find ../BD1/Split/0/*.png   | head -n $$((15*$$number)) | tail -n 150` 0/ ; \
		ls 0 | wc -l ; \
		ls 1 | wc -l ; \
		train ; \
		clasify Originales/input.png Originales/clasif.png 20; \
		\cp output.png Error/output.png ; \
		\cp output.png Error/Train1/output-$$number.png ; \
		estadistica Error/output.png Originales/clasif.png ; \
	done
	mv Error/resultados Error/Train1

plot:
	gnuplot script_gnuplot && eog cells.png

