#include <stdio.h>
#include <stdlib.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>




int main( int argc, char** argv ) {
	
	FILE *fichero;
	IplImage* output  = cvLoadImage(argv[1], 0);
	IplImage* clasif = cvLoadImage(argv[2], 0);
	int x, y, error = 0, tam;
	float px, py, grad, porcent;

	for(x=0; x<(output->height); x++) {
		for(y=0; y<(output->width); y++) {
				
			px = cvGetReal2D(output,x,y);
			py = cvGetReal2D(clasif,x,y);
			
			if(px!=py){
				error++;
			}
				
		}
	}

	tam = (output->height) * (output->width);
	
	porcent =((float)error/(float)tam)*100;
	printf("error: %d \n", error);
	printf("tam: %d \n", tam);

	printf("error percent: %f \n", porcent);

	fichero = fopen("Error/resultados", "a");
	fprintf(fichero,"%f\n",porcent);

return 0;
}
