#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <math.h>
#include <highgui.h>
#include <vector>
#include "opencv2/imgproc/imgproc.hpp"

#define MAX_T 100
#define MAX_CELLS 500
#define N_ITER 25
#define LENGTH_CELL 17
#define gamma 0.001
#define PI 3.141592

using namespace std;
using namespace cv;

typedef struct {
	int x;
	int y;
	float r;
} Cell;

float energy(Cell cell, IplImage* input_image){

	int min_x = cell.x - LENGTH_CELL/2 * cos(cell.r) - 5;
	int max_x = cell.x + LENGTH_CELL/2 * cos(cell.r) + 5;
	int min_y = cell.y - LENGTH_CELL/2 * sin(cell.r) - 5;
	int max_y = cell.y + LENGTH_CELL/2 * sin(cell.r) + 5;

	if(min_x > max_x){
		int tmp = min_x;
		min_x = max_x;
		max_x = tmp;
	}

	if(min_y > max_y){
		int tmp = min_y;
		min_y = max_y;
		max_y = tmp;
	}

	if(min_x < 0) min_x = 0; 
	if(min_x >= input_image->width) min_x = input_image->width-1;
	
	if(max_x < 0) max_x = 0; 
	if(max_x >= input_image->width) max_x = input_image->width-1;

	if(min_y < 0) min_y = 0; 
	if(min_y >= input_image->height) min_y = input_image->height-1;
		
	if(max_y < 0) max_y = 0; 
	if(max_y >= input_image->height) max_y = input_image->height-1;

	IplImage* output = cvCreateImage(cvSize(input_image->width,input_image->height), IPL_DEPTH_8U, 1);
	cvCopy(input_image, output);

	int pixels_white_before = 0;
	for ( unsigned int x = min_x; x < max_x; x++) {
		for ( unsigned int y = min_y; y < max_y; y++) {
			if(cvGetReal2D(output,y,x)){
				pixels_white_before++;
			}	
		}
	}

	int point_1_x = cell.x-(LENGTH_CELL/2)*cos(cell.r);
	int point_1_y = cell.y-(LENGTH_CELL/2)*sin(cell.r);
	int point_2_x = cell.x+(LENGTH_CELL/2)*cos(cell.r);
	int point_2_y = cell.y+(LENGTH_CELL/2)*sin(cell.r);

	cvLine(output, CvPoint(point_1_x,point_1_y), CvPoint (point_2_x, point_2_y), CV_RGB(0,0,0), 3);

	int pixels_white_after = 0;
	for ( unsigned int x = min_x; x < max_x; x++) {
		for ( unsigned int y = min_y; y < max_y; y++) {
			if(cvGetReal2D(output,y,x)){
				pixels_white_after++;
			}	
		}
	}

	cvReleaseImage(&output);

	return pixels_white_before - pixels_white_after;

}

float compute_delta_e(Cell cell, Cell* new_cell, IplImage* input_image){
	return -(energy(*new_cell, input_image) - energy(cell, input_image));
}

void modify_cell(Cell cell, Cell* new_cell, float* delta_E, float step, IplImage* input_image){

	new_cell->x = cell.x + ((rand() % 50) - 20 )   * exp(-gamma*step);
	new_cell->y = cell.y + ((rand() % 50) - 20 )   * exp(-gamma*step);
	new_cell->r =(float)(cell.r + (rand() % 180)*(PI/180.0) * exp(-gamma*step));

	/*new_cell->x = cell.x + ((rand() % 50) - 25 )   * step;
	new_cell->y = cell.y + ((rand() % 50) - 25 )   * step;
	new_cell->r =(float)(cell.r + (rand() % 180)*(PI/180.0) * step);*/


	if(new_cell->x > input_image->width)  new_cell->x = input_image->width;
	if(new_cell->y > input_image->height) new_cell->y = input_image->height;
	if(new_cell->x < 0) new_cell->x = 0;
	if(new_cell->y < 0) new_cell->y = 0;

	if(new_cell->r > PI ) new_cell->r = new_cell->r - PI;
	if(new_cell->r < 0   ) new_cell->r = new_cell->r + PI;

	*delta_E = compute_delta_e(cell, new_cell, input_image);
}

void paint_cell(Cell cell_to_paint, IplImage* imagen){
	int point_1_x = cell_to_paint.x-(LENGTH_CELL/2)*cos(cell_to_paint.r);
	int point_1_y = cell_to_paint.y-(LENGTH_CELL/2)*sin(cell_to_paint.r);
	int point_2_x = cell_to_paint.x+(LENGTH_CELL/2)*cos(cell_to_paint.r);
	int point_2_y = cell_to_paint.y+(LENGTH_CELL/2)*sin(cell_to_paint.r); 
	cvLine(imagen, CvPoint(point_1_x,point_1_y), CvPoint (point_2_x, point_2_y), CV_RGB(0,0,0), 3);	
}

void paint_color_cell(Cell cell_to_paint, IplImage* imagen){
	int point_1_x = cell_to_paint.x-(LENGTH_CELL/2)*cos(cell_to_paint.r);
	int point_1_y = cell_to_paint.y-(LENGTH_CELL/2)*sin(cell_to_paint.r);
	int point_2_x = cell_to_paint.x+(LENGTH_CELL/2)*cos(cell_to_paint.r);
	int point_2_y = cell_to_paint.y+(LENGTH_CELL/2)*sin(cell_to_paint.r); 
	cvLine(imagen, CvPoint(point_1_x,point_1_y), CvPoint (point_2_x, point_2_y), CV_RGB(0,255,0), 3);	
}

int main(int argc, const char *argv[]){
	
	srand(time(NULL));
	
	Cell cell;
	Cell new_cell;
	float delta_E;
	int step,n;

	vector<Cell> final_cells;

	IplImage* input = cvLoadImage(argv[1], 0);
	IplImage* output = cvCreateImage(cvSize(input->width,input->height), IPL_DEPTH_8U, 3);

	cvCvtColor(input, output, CV_GRAY2RGB);

	for (n = 0; n < 1000; n++) {

		cell.x = 0; cell.y = 0;
		while(!cvGetReal2D(input,cell.y,cell.x)){
			cell.x = rand() % ( input->width - 1 );
			cell.y = rand() % ( input->height - 1 );
		}

		for(step=0; step < 100000; step++){

			modify_cell(cell, &new_cell, &delta_E, step, input);

			if(delta_E < 0){
				cell = new_cell;
			}


		}

		if(energy(cell, input) > 60 ){
			paint_cell(cell, input);
			paint_color_cell(cell, output);
			final_cells.push_back(cell);
		}


		cvShowImage( "Input",  input);
		cvShowImage( "Output", output);
		cvWaitKey(10);
	}




	cvReleaseImage(&input);
	return 0;

}

