/*
 * =====================================================================================
 * /
 * |     Filename:  mod.cpp
 * |
 * |  Description:  
 * |
 * |      Version:  1.0
 * |      Created:  02/29/2016 03:58:05 PM
 * |     Revision:  none
 * |     Compiler:  gcc
 * `-. .--------------------
 *    Y
 *    ,,  ,---,
 *   (_,\/_\_/_\     Author:   Pablo González de Aledo (), pablo.aledo@gmail.com
 *     \.\_/_\_/>    Company:  Universidad de Cantabria
 *     '-'   '-'
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <fstream>

using namespace std;

vector<string> tokenize(const string& str,const string& delimiters) {
	vector<string> tokens;
    	
	// skip delimiters at beginning.
    	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
    	string::size_type pos = str.find_first_of(delimiters, lastPos);

    	while (string::npos != pos || string::npos != lastPos)
    	{
		// found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		// skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
    	}

	return tokens;
}

int stoi(string str){
	int ret;
	sscanf(str.c_str(), "%d", &ret);
	return ret;
}

int nsamples;
int components;
int threshold;

void output(){
	char str[10];

	FILE* filein  = fopen("/tmp/clasif_file","r");
	FILE* fileout = fopen("/tmp/output_file","w");


	for ( unsigned int n = 0; n < nsamples; n++) {
		int mod = 0;
		fscanf(filein,"%s", str);
		for ( unsigned int i = 0; i < components; i++) {
			fscanf(filein,"%s", str);
			int num = stoi(tokenize(string(str),":")[1]);
			mod += num*num;
		}

		if(mod > threshold) fprintf(fileout, "1\n");
		else              fprintf(fileout, "0\n");


	}
	fclose(filein);
	fclose(fileout);

}

void get_params(){

	ifstream input("/tmp/clasif_file");
	string line;

	getline(input,line);
	components = tokenize(line, " ").size()-1;
	int lines = 1;
	
	while( getline( input, line ) ) {
		lines++;
	}
	
	nsamples = lines;

}

int main(int argc, const char *argv[])
{
	get_params();
	threshold = atoi(argv[1]);

	output();
	return 0;
}

