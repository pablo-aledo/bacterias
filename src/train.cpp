/*
 * =====================================================================================
 * /
 * |     Filename:  train.cpp
 * |
 * |  Description:  
 * |
 * |      Version:  1.0
 * |      Created:  02/25/2016 06:10:44 PM
 * |     Revision:  none
 * |     Compiler:  gcc
 * `-. .--------------------
 *    Y
 *    ,,  ,---,
 *   (_,\/_\_/_\     Author:   Pablo González de Aledo (), pablo.aledo@gmail.com
 *     \.\_/_\_/>    Company:  Universidad de Cantabria
 *     '-'   '-'
 * =====================================================================================
 */


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>

using namespace cv;
using namespace std;

vector<string> tokenize(const string& str,const string& delimiters) {
	vector<string> tokens;
    	
	// skip delimiters at beginning.
    	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
    	string::size_type pos = str.find_first_of(delimiters, lastPos);

    	while (string::npos != pos || string::npos != lastPos)
    	{
		// found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		// skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
    	}

	return tokens;
}

int main( int argc, char** argv ) {


	system("find 1/* 0/* > /tmp/list");
	
	ifstream inputlist("/tmp/list");
	string line;
	
	FILE* trainfile = fopen("/tmp/trainfile", "w");

	while( getline( inputlist, line ) ) {
		string clasif = tokenize(line, "/")[0];
		string imgfile = line;

		fprintf(trainfile, "%s ", clasif.c_str());

		IplImage* input  = cvLoadImage(imgfile.c_str(), 0);


		int avg = 0;
		for ( unsigned int y = 0; y < input->height; y++) {
			for ( unsigned int x = 0; x < input->width; x++) {
				int pixel = CV_IMAGE_ELEM(input, unsigned char, y,x);
				avg += pixel;
			}
		}
		avg /= (input->width*input->height);


		int n = 0;
		for ( unsigned int y = 0; y < input->height; y++) {
			for ( unsigned int x = 0; x < input->width; x++) {
				int pixel = CV_IMAGE_ELEM(input, unsigned char, y,x);
				n++;
				fprintf(trainfile, "%d:%d ", n, pixel-avg );
			}
		}
		fprintf(trainfile, "\n");


		cvReleaseImage(&input);

	}

	fclose(trainfile);


	//system("svm-train -s 0 -c 10 -t 1 -g 1 -r 1 -d 3 /tmp/trainfile model");
	system("pca train /tmp/trainfile 50");
	system("pca project /tmp/trainfile 50");
	//system("svm-train /tmp/trainfile model");

	return 0;
}
