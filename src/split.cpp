/*
 * =====================================================================================
 * /
 * |     Filename:  split.cpp
 * |
 * |  Description:  
 * |
 * |      Version:  1.0
 * |      Created:  02/25/2016 05:15:02 PM
 * |     Revision:  none
 * |     Compiler:  gcc
 * `-. .--------------------
 *    Y
 *    ,,  ,---,
 *   (_,\/_\_/_\     Author:   Pablo González de Aledo (), pablo.aledo@gmail.com
 *     \.\_/_\_/>    Company:  Universidad de Cantabria
 *     '-'   '-'
 * =====================================================================================
 */


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <math.h>

using namespace cv;
using namespace std;

IplImage* rotate(IplImage* src, IplImage* dst, float angle);

string itos(int i){
	stringstream i_ss;
	i_ss << i;
	return i_ss.str();
}

int stof(string str){
	float ret;
	sscanf(str.c_str(), "%f", &ret);
	return ret;
}


int main( int argc, char** argv ) {

	srand(time(NULL));

	IplImage* input          = cvLoadImage(argv[1], 0);
	IplImage* input_rotated  = cvLoadImage(argv[1], 0);
	IplImage* clasif         = cvLoadImage(argv[2], 0);
	IplImage* clasif_rotated = cvLoadImage(argv[2], 0);
	int templatesize         = atoi(argv[3]);
	float angle              = stof(string(argv[4]));

	rotate(input, input_rotated, angle);
	rotate(clasif, clasif_rotated, angle);

	//cvSaveImage("output.png", clasif_rotated);
	//exit(0);

	for ( unsigned int y = 0; y < input_rotated->height-templatesize; y++) {
		for ( unsigned int x = 0; x < input_rotated->width-templatesize; x++) {

			int pixelclasif = CV_IMAGE_ELEM(clasif_rotated, unsigned char, y+templatesize/2,x+templatesize/2);

			CvRect rect;
			rect.x      = x;
			rect.y      = y;
			rect.width  = templatesize;
			rect.height = templatesize;

			cvSetImageROI(input_rotated, rect);

			if((pixelclasif && rand()%1000 > 850) || (!pixelclasif && rand()%1000 > 960)){
				string filename = string(pixelclasif?"./1/":"./0/") + itos(rand()) + ".png";
				cvSaveImage( filename.c_str(), input_rotated);
			}

		}
	}

	return 0;
}



IplImage* rotate(IplImage* src, IplImage* dst, float angle){

	float m[6];
	CvMat M = cvMat(2, 3, CV_32F, m);
	int w = src->width;
	int h = src->height;
	cv2DRotationMatrix(cvPoint2D32f(w/2,h/2), angle, 1., &M);

	cvWarpAffine( src, dst, &M, CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS, cvScalarAll(0));
}


