/*
 * =====================================================================================
 * /
 * |     Filename:  clasify.cpp
 * |
 * |  Description:  
 * |
 * |      Version:  1.0
 * |      Created:  02/25/2016 06:32:27 PM
 * |     Revision:  none
 * |     Compiler:  gcc
 * `-. .--------------------
 *    Y
 *    ,,  ,---,
 *   (_,\/_\_/_\     Author:   Pablo González de Aledo (), pablo.aledo@gmail.com
 *     \.\_/_\_/>    Company:  Universidad de Cantabria
 *     '-'   '-'
 * =====================================================================================
 */



#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>

using namespace cv;
using namespace std;

int main( int argc, char** argv ) {


	IplImage* input  = cvLoadImage(argv[1], 0);
	IplImage* ground = cvLoadImage(argv[2], 0);
	int templatesize = atoi(argv[3]);

	FILE* clasif_file = fopen("/tmp/clasif_file", "w");

	for ( unsigned int y = 0; y < input->height-templatesize; y++) {
		for ( unsigned int x = 0; x < input->width-templatesize; x++) {

			int grnd = CV_IMAGE_ELEM(ground, unsigned char, y+templatesize/2,x+templatesize/2);

			int n = 0;

			fprintf(clasif_file, "%d ", grnd?1:0);



			int avg = 0;
			for ( unsigned int yy = 0; yy < templatesize; yy++) {
				for ( unsigned int xx = 0; xx < templatesize; xx++) {
					int pixel = CV_IMAGE_ELEM(input, unsigned char, y+yy,x+xx);
					avg += pixel;
				}
			}
			avg /= (templatesize*templatesize);

			for ( unsigned int yy = 0; yy < templatesize; yy++) {
				for ( unsigned int xx = 0; xx < templatesize; xx++) {

					int pixelclasif = CV_IMAGE_ELEM(input, unsigned char, y+yy, x+xx);

					n++;
					fprintf(clasif_file, "%d:%d ", n, pixelclasif-avg);
				}
			}
			fprintf(clasif_file, "\n");

		}
	}
	fclose(clasif_file);


	//system("svm-predict /tmp/clasif_file model /tmp/output_file");
	system("pca project /tmp/clasif_file 50");
	//system("svm-predict /tmp/clasif_file model /tmp/output_file");
	//system("cd ~/libsvm-3.211/tools/; ./easy.py /tmp/trainfile /tmp/clasif_file; mv clasif_file.predict /tmp/output_file");
	system("svm-scale -r Ficheros_Entrenamiento/trainfile.range /tmp/clasif_file > /tmp/clasif_file_scaled");
	system("svm-predict /tmp/clasif_file_scaled Ficheros_Entrenamiento/trainfile.model /tmp/output_file");
	//system("mod 3900000");

	IplImage* output = cvCreateImage(cvSize(input->width, input->height), IPL_DEPTH_8U,1);
	cvSet(output, CV_RGB(0,0,0));
	FILE* inputfile = fopen("/tmp/output_file", "r");




	for ( unsigned int y = 0; y < input->height-templatesize; y++) {
		for ( unsigned int x = 0; x < input->width-templatesize; x++) {
			int value;
			int yy = y + templatesize/2;
			int xx = x + templatesize/2;
			fscanf(inputfile, "%d", &value);
			uchar* ptr = (uchar*) ( output->imageData + yy * output->widthStep + xx);
			*ptr = value*255;
		}
	}


	cvSaveImage( "output.png", output);

	return 0;
}

