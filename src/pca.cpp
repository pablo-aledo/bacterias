/*
 * =====================================================================================
 * /
 * |     Filename:  pca.cpp
 * |
 * |  Description:  
 * |
 * |      Version:  1.0
 * |      Created:  02/28/2016 10:47:42 AM
 * |     Revision:  none
 * |     Compiler:  gcc
 * `-. .--------------------
 *    Y
 *    ,,  ,---,
 *   (_,\/_\_/_\     Author:   Pablo González de Aledo (), pablo.aledo@gmail.com
 *     \.\_/_\_/>    Company:  Universidad de Cantabria
 *     '-'   '-'
 * =====================================================================================
 */

#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

vector<string> tokenize(const string& str,const string& delimiters) {
	vector<string> tokens;
    	
	// skip delimiters at beginning.
    	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
    	string::size_type pos = str.find_first_of(delimiters, lastPos);

    	while (string::npos != pos || string::npos != lastPos)
    	{
		// found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		// skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
    	}

	return tokens;
}

int stoi(string str){
	int ret;
	sscanf(str.c_str(), "%d", &ret);
	return ret;
}

int nsamples;
int templatesize;
int components;
string filename;

void train(){

	Mat src = Mat::zeros(nsamples, templatesize*templatesize, CV_32F);
		
	FILE* file = fopen(filename.c_str(),"r");
	char str[10];

	for ( unsigned int n = 0; n < nsamples; n++) {
		fscanf(file,"%s", str);
		for ( unsigned int i = 0; i < templatesize*templatesize; i++) {
			fscanf(file,"%s", str);
			int num = stoi(tokenize(string(str),":")[1]);
			src.at<float>(n,i) = num;
		}
	}
	fclose(file);

	PCA pca(src, Mat(),PCA::DATA_AS_ROW,components);
	FileStorage fs("pca_analysis", FileStorage::WRITE);
	pca.write(fs);

}

void project(){
	

	Mat src = Mat::zeros(nsamples, templatesize*templatesize, CV_32F);
	PCA pca;
	FileStorage fs("Ficheros_Entrenamiento/pca_analysis", FileStorage::READ);
	pca.read(fs.root());
	char str[10];
	vector<int> outputs;


	FILE* file = fopen(filename.c_str(),"r");
	for ( unsigned int n = 0; n < nsamples; n++) {
		int num;
		fscanf(file,"%d", &num);
		outputs.push_back(num);
		for ( unsigned int i = 0; i < templatesize*templatesize; i++) {
			fscanf(file,"%s", str);
			num = stoi(tokenize(string(str),":")[1]);
			src.at<float>(n,i) = num;
		}
	}
	fclose(file);

	file = fopen(filename.c_str(),"w");
	for ( unsigned int n = 0; n < nsamples; n++) {
		Mat projection = pca.project(src.row(n));
		fprintf(file, "%d ", outputs[n]);
		for ( int i = 0; i < components; i++) {
			int component = projection.at<float>(i);
			fprintf(file,"%d:%d ", i+1, component);
		}
		fprintf(file, "\n");
	}
	fclose(file);

}

void get_params(){

	ifstream input(filename.c_str());
	string line;

	getline(input,line);
	templatesize = sqrt(tokenize(line, " ").size()-1);
	int lines = 1;
	
	while( getline( input, line ) ) {
		lines++;
	}
	
	nsamples = lines;

}

int main(int argc, const char *argv[])
{

	string action = string(argv[1]);
	filename      = string(argv[2]);
	get_params();
	components    =   atoi(argv[3]);

	printf("nsamples %d templatesize %d\n", nsamples, templatesize);
	//exit(0);

	if(!strcmp(argv[1],"train")) train();
	else project();
	
	return 0;
}

