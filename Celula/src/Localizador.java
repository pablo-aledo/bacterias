import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

import javax.imageio.ImageIO;

public class Localizador {

	static int height;
	static int width;
	static int[][] array2D;
	static int[][] arrayConBordes;
	
	static int[][] arraySegundaPasada;
	static LinkedList <Celula> cels;
	public static void main(String args[]){
		
		try {
			//String fileName= "../clasif22.png";
			//String fileName= "../exor.png";
			//String fileName= "../exor2.png";
			String fileName= "../exor3chan.png";
			//String fileName= "../output-7.png";
			//String fileName= "../umbral.png";
			seeBMPImage (fileName);
			
			cels = new LinkedList <Celula>();
			System.out.println("Primera Pasada:");
			System.out.println("-----Buscando Bordes");
			buscaBordes();
			//buscaBordesSegunda();
			//pinta();
			//pintaFinal2();
			System.out.println("-----Buscando Celulas");
			buscaCelulas();
			System.out.println("-----Borrando Celulas pequenas");
			borraCelulasPequenas(15);
			
			System.out.println("Segunda Pasada:");
			copiaArrayToSegunda();
			System.out.println("-----Eliminando Celulas ya detectadas");
			borraCelulasExistentesSegundaPasada();
			System.out.println("-----Buscando Celulas con segundo metodo");
			buscaCelulasSegundaPasada();
			System.out.println("-----Borrando Celulas pequenas");
			borraCelulasPequenas(10);
			
			System.out.println("Tercera Pasada:");
			System.out.println("-----Procesando Celulas demasiado grandes");
			ProcesaCelulasGrandes3(80,8);
			
			//IDEA!!!! Unir celulas pequenas cercanas???
			
			System.out.println("-----Borrando Celulas pequenas");
			borraCelulasPequenas(6);
			
			
			//Tercera Busqueda....
			System.out.println("Cuarta Pasada:");
			copiaArrayToSegunda();
			System.out.println("-----Eliminando Celulas ya detectadas (extrema)");
			borraCelulasExistentesSegundaPasada_Gordas();
			System.out.println("-----Buscando Celulas con segundo metodo");
			buscaCelulasSegundaPasada();
			System.out.println("-----Borrando Celulas pequenas");
			borraCelulasPequenas(10);
			//fin de tercera busqueda
			
			//pintaFinal();
			System.out.println("FINALIZANDO");
			System.out.println("-->Numero de celulas encontradas:" + cels.size());
			System.out.println("-->pintando en saved.png la salida...");
			pintaCelulasToBMPImage(fileName);
			
			/*int numCels=0;
			for (int i=0;i<cels.size();i++){
				if(cels.get(i).getlPixels().size()>5){
					numCels++;
				}
			}
			System.out.println("numero de celulas truncadas:" + numCels);*/
			//.out.println("Numero de celulas encontradas:" + cels.size());
			
			/* byte[] image=extractBytes("clasif22.png");
			for(int i=0;i<image.length;i++){
				System.out.println(image[i]);
			}*/
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static LinkedList <Pixel> listPixelsAnadidos;
	
	
	public static void pintaCelulasToBMPImage2(LinkedList<Celula> lista) throws IOException {
	    //BufferedImage image = ImageIO.read(getClass().getResource(BMPFileName));
		BufferedImage image = ImageIO.read(new File("../exor3chan.png"));
		for(int i=0;i<lista.size();i++){
			Celula c= lista.get(i);
			//color = getIntFromColor((int)Math.random()*250,(int)Math.random()*250,(int)Math.random()*250);
			//color = getIntFromColor((int)Math.random()*250,(int)Math.random()*250,(int)Math.random()*250);
			color = getIntFromColor((int)Math.floor(Math.random()*255+1),(int)Math.floor(Math.random()*255+1),(int)Math.floor(Math.random()*255+1));
			//color = getIntFromColor(255,0,255);
			//System.out.println(color);
			for(int j=0;j<c.getlPixels().size();j++){
				Pixel p=c.getlPixels().get(j);
				image.setRGB(p.getX(), p.getY(), color );
				/*image.setRGB(p.getX()-1, p.getY()-1, color );
				image.setRGB(p.getX()-1, p.getY(), color );
				image.setRGB(p.getX()-1, p.getY()+1, color );
				image.setRGB(p.getX(), p.getY()-1, color );
				image.setRGB(p.getX(), p.getY(), color );
				image.setRGB(p.getX(), p.getY()+1, color );
				image.setRGB(p.getX()+1, p.getY()-1, color );
				image.setRGB(p.getX()+1, p.getY(), color );
				image.setRGB(p.getX()+1, p.getY()+1, color );*/
			}
			//image.setRGB(c.getCentroX(), c.getCentroY(), getIntFromColor(255,0,0));
			//color+= 1;
		}
	    File outputfile = new File("../saved2.png");
	    ImageIO.write(image, "png", outputfile);
	 }
	
	public static void ProcesaCelulasGrandes3(int tamanoMaximo,int numVecinos) throws IOException {
		LinkedList<Celula> listaModificadas= new LinkedList<Celula>();
		//elimino los bordes de las celulas grandes
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			if(c.getlPixels().size()>tamanoMaximo){
				
				LinkedList<Pixel> listaEliminados=new LinkedList<Pixel>();
				for(int j=0;j<c.getlPixels().size();j++){
					Pixel p=c.getlPixels().get(j);
					if(numVecinos(p, c.getlPixels())<numVecinos){
						listaEliminados.add(c.getlPixels().get(j));
						//c.getlPixels().remove(j);
						//j--;
					}
				}
				for(int k=0;k<listaEliminados.size();k++){
					c.getlPixels().remove(listaEliminados.get(k));
				}
				listaModificadas.add(c);
				cels.remove(i);//DEBO BORRAR LAS GRANDES Y ANADIR LAS PEQUENAS....
				i--;
			}
			
		}
		
		pintaCelulasToBMPImage2(listaModificadas);
		
		//agrupo los cachos de las celulas grandes con las que tienen vecinos...
		//LinkedList<Celula> listaCelsEliminadas=new LinkedList<Celula>();
		LinkedList<Celula> listaCelsNuevas=new LinkedList<Celula>();
		listPixelsAnadidos=new LinkedList<Pixel>();
		//int f=0;
		for(int i=0;i<listaModificadas.size();i++){
			Celula c= listaModificadas.get(i);
			listPixelsAnadidos.clear();
				//System.out.println("celdas grandes"+ f);
				//f++;
				
				//Celula c2=new Celula(0,0,0,0,true);
				for(int j=0;j<c.getlPixels().size();j++){
					Pixel p=c.getlPixels().get(j);
					if(anadido(p,listPixelsAnadidos)){continue;}
					Celula c2=new Celula(0,0,0,0,true);
					c2.getlPixels().add(new Pixel(p.getX(),p.getY(),p.getColor()));
					
					
					//buscaVecinosCelulasGrandes(p,c,c2,listPixelsAnadidos,j);
					buscaVecinosCelulasGrandes2(p.getX(),p.getY(),c,c2);
					listPixelsAnadidos.add(p);
					listaCelsNuevas.add(c2);
				}
				//cels.remove(i);
				//i--;
				//listaCelsEliminadas.add(c);
			}		
		
		/*for(int k=0;k<listaCelsEliminadas.size();k++){
			//System.out.println("elimino celda"+ k+ "size"+cels.size());
			cels.remove(listaCelsEliminadas.get(k));
		}*/
		
		for(int k=0;k<listaCelsNuevas.size();k++){
			System.out.println("celdanueva "+ k+ "size"+cels.size());
			cels.add(listaCelsNuevas.get(k));
		}
		
	 }
	
	
	public static void ProcesaCelulasGrandes(int tamanoMaximo,int numVecinos) throws IOException {

		//elimino los bordes de las celulas grandes
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			if(c.getlPixels().size()>tamanoMaximo){
				LinkedList<Pixel> listaEliminados=new LinkedList<Pixel>();
				for(int j=0;j<c.getlPixels().size();j++){
					Pixel p=c.getlPixels().get(j);
					if(numVecinos(p, c.getlPixels())<numVecinos){
						listaEliminados.add(c.getlPixels().get(j));
						//c.getlPixels().remove(j);
						//j--;
					}
				}
				for(int k=0;k<listaEliminados.size();k++){
					c.getlPixels().remove(listaEliminados.get(k));
				}
			}
			
		}
		/*
		//agrupo los cachos de las celulas grandes con las que tienen vecinos...
		LinkedList<Celula> listaCelsEliminadas=new LinkedList<Celula>();
		LinkedList<Celula> listaCelsNuevas=new LinkedList<Celula>();
		listPixelsAnadidos=new LinkedList<Pixel>();
		int f=0;
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			listPixelsAnadidos.clear();
			if(c.getlPixels().size()>tamanoMaximo){
				System.out.println("celdas grandes"+ f);
				f++;
				
				//Celula c2=new Celula(0,0,0,0,true);
				for(int j=0;j<c.getlPixels().size();j++){
					Pixel p=c.getlPixels().get(j);
					if(anadido(p,listPixelsAnadidos)){continue;}
					Celula c2=new Celula(0,0,0,0,true);
					c2.getlPixels().add(new Pixel(p.getX(),p.getY(),p.getColor()));
					
					
					//buscaVecinosCelulasGrandes(p,c,c2,listPixelsAnadidos,j);
					buscaVecinosCelulasGrandes2(p.getX(),p.getY(),c,c2);
					listPixelsAnadidos.add(p);
					listaCelsNuevas.add(c2);
				}
				//cels.remove(i);
				//i--;
				listaCelsEliminadas.add(c);
			}
			
		}
		
		
		for(int k=0;k<listaCelsEliminadas.size();k++){
			//System.out.println("elimino celda"+ k+ "size"+cels.size());
			cels.remove(listaCelsEliminadas.get(k));
		}
		*/
		/*for(int k=0;k<listaCelsNuevas.size();k++){
			System.out.println("celdanueva "+ k+ "size"+cels.size());
			cels.add(listaCelsNuevas.get(k));
		}*/
		
	 }
	
	public static void buscaVecinosCelulasGrandes2(int x, int y, Celula c, Celula c2){
		//System.out.println("num="+c2.getlPixels().size());
		if(x>0 && y>0 && estaEnLista(x-1,y-1,c.getlPixels())&& !estaEnListaAnadidos(x-1,y-1,c.getlPixels())){c2.getlPixels().add(new Pixel(x-1,y-1,0)); buscaVecinosCelulasGrandes2(x-1,y-1,c,c2);}
		if(x>0 && estaEnLista(x-1,y,c.getlPixels())&& !estaEnListaAnadidos(x-1,y,c.getlPixels())){c2.getlPixels().add(new Pixel(x-1,y,0)); buscaVecinosCelulasGrandes2(x-1,y,c,c2);}
		if(x>0 && y<height-1 && estaEnLista(x-1,y+1,c.getlPixels())&& !estaEnListaAnadidos(x-1,y+1,c.getlPixels())){c2.getlPixels().add(new Pixel(x-1,y+1,0)); buscaVecinosCelulasGrandes2(x-1,y+1,c,c2);}
		if(y>0 && estaEnLista(x,y-1,c.getlPixels())&& !estaEnListaAnadidos(x,y-1,c.getlPixels())){c2.getlPixels().add(new Pixel(x,y-1,0)); buscaVecinosCelulasGrandes2(x,y-1,c,c2);}
		if(y<height-1 && estaEnLista(x,y+1,c.getlPixels())&& !estaEnListaAnadidos(x,y+1,c.getlPixels())){c2.getlPixels().add(new Pixel(x,y+1,0)); buscaVecinosCelulasGrandes2(x,y+1,c,c2);}
		if(x<width-1 && y>0 && estaEnLista(x+1,y-1,c.getlPixels())&& !estaEnListaAnadidos(x+1,y-1,c.getlPixels())){c2.getlPixels().add(new Pixel(x+1,y-1,0)); buscaVecinosCelulasGrandes2(x+1,y-1,c,c2);}
		if(x<width-1 && estaEnLista(x+1,y,c.getlPixels())&& !estaEnListaAnadidos(x+1,y,c.getlPixels())){c2.getlPixels().add(new Pixel(x+1,y,0)); buscaVecinosCelulasGrandes2(x+1,y,c,c2);}
		if( x<width-1 && estaEnLista(x+1,y+1,c.getlPixels())&& !estaEnListaAnadidos(x+1,y+1,c.getlPixels())){c2.getlPixels().add(new Pixel(x+1,y+1,0)); buscaVecinosCelulasGrandes2(x+1,y+1,c,c2);}
	}
	
	
	public static boolean estaEnLista(int x,int y,LinkedList <Pixel> listac ){
		for(int i=0;i<listac.size()-1;i++){
			Pixel p = listac.get(i);
			if(p.getX()==x && p.getY()==y){
				listPixelsAnadidos.add(listac.get(i));
				return true;
			}
		}
		return false;
	}
	
	public static boolean estaEnListaAnadidos(int x,int y,LinkedList <Pixel> listac ){
		for(int i=0;i<listPixelsAnadidos.size()-1;i++){
			Pixel p = listPixelsAnadidos.get(i);
			if(p.getX()==x && p.getY()==y){
				return true;
			}
		}
		return false;
	}
	
	public static void buscaVecinosCelulasGrandes(Pixel p, Celula c, Celula c2, LinkedList <Pixel> listPixelsAnadidos,int l){
		for(int k=l;k<c.getlPixels().size();k++){
			Pixel p2=c.getlPixels().get(k);
			if(sonVecinos(p,p2) && !anadido(p2,listPixelsAnadidos)){
				c2.getlPixels().add(new Pixel(p2.getX(),p2.getY(),p2.getColor()));
				listPixelsAnadidos.add(p2);
				buscaVecinosCelulasGrandes(p2,c,c2,listPixelsAnadidos,k);
			}
		}
	}
	
	public static boolean anadido(Pixel p, LinkedList <Pixel> listPixels){
		for(int i=0;i<listPixels.size();i++){
			if(p.getX()==listPixels.get(i).getX() && p.getY()==listPixels.get(i).getY()){
				return true;
			}
		}
		return false;
	}
	
	public static boolean sonVecinos(Pixel p, Pixel p2){
		if(((p.getX()<=(p2.getX()+1))&&(p.getX()>=(p2.getX()-1))) && ((p.getY()<=(p2.getY()+1))&&(p.getY()>=(p2.getY()-1))) ){
			//System.out.println(""+p.getX() +"," + p.getY()+" - "+ p2.getX()+ ","+ p.getY());
			return true;
		}
		return false;
	}
	
	/*public static void ProcesaCelulasGrandes(int tamanoMaximo) throws IOException {
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			if(c.getlPixels().size()>tamanoMaximo){
				Celula cnueva=new Celula(0,0,0,0, true);
				for(int j=0;j<c.getlPixels().size();j++){
					
					Pixel p=c.getlPixels().get(j);
					if(numVecinos(p, c.getlPixels())<5){
						Celula cnueva2=new Celula(0,0,0,0,true);
						for(int k=0;k<cnueva.getlPixels().size();k++){
							cnueva2.getlPixels().add(new Pixel(cnueva.getlPixels().get(k).getX(),cnueva.getlPixels().get(k).getY(),cnueva.getlPixels().get(k).getColor()));
						}
						cels.add(cnueva2);
						cnueva=new Celula(0,0,0,0, true);
					}else{
						cnueva.getlPixels().add(p);
					}
				}
				cels.add(cnueva);					
			}
			cels.remove(i);
		}
	 }*/
	
	public static int numVecinos(Pixel p,  LinkedList<Pixel> lista){
		int num=0;
		for(int i=0;i<lista.size();i++){
			Pixel p2=lista.get(i);
			if(((p.getX()<=(p2.getX()+1))&&(p.getX()>=(p2.getX()-1))) && ((p.getY()<=(p2.getY()+1))&&(p.getY()>=(p2.getY()-1))) ){
				num++;
			}
		}
		//System.out.println("num vecinos"+num);
		return num;
	}
	
	
	public static void copiaArrayToSegunda(){
		arraySegundaPasada = new int[width][height];
		for(int x=1;x<width-1;x++){
			for(int y=1;y<height-1;y++){
				arraySegundaPasada[x][y]=array2D[x][y];
			}
		}
	}
	
	
	
	
	public static void borraCelulasExistentesSegundaPasada() throws IOException {
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			for(int j=0;j<c.getlPixels().size();j++){
				Pixel p=c.getlPixels().get(j);
				int x = p.getX();
				int y = p.getY();
				
				if(x>0 && y>0 ){		    arraySegundaPasada[p.getX()-1][p.getY()-1]=1;}
				if(x>0){		  		    arraySegundaPasada[p.getX()-1][p.getY()]=1;}
				if(x>0 && y<height-1 ){     arraySegundaPasada[p.getX()-1][p.getY()+1]=1;}
				if( y>0 ){				    arraySegundaPasada[p.getX()][p.getY()-1]=1;}
				if(true ){				    arraySegundaPasada[p.getX()][p.getY()]=1;}
				if(y<height-1){      		arraySegundaPasada[p.getX()][p.getY()+1]=1;}
				if(x<width-1 && y>0 ){      arraySegundaPasada[p.getX()+1][p.getY()-1]=1;}
				if(x<width-1 ){ 		   	arraySegundaPasada[p.getX()+1][p.getY()]=1;}
				if(x<width-1 && y<height-1){arraySegundaPasada[p.getX()+1][p.getY()+1]=1;}
			}
		}
	 }
	
	public static void borraCelulasExistentesSegundaPasada_Gordas() throws IOException {
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			for(int j=0;j<c.getlPixels().size();j++){
				Pixel p=c.getlPixels().get(j);
				int x = p.getX();
				int y = p.getY();
				if(x>0 && y>0 ){		    arraySegundaPasada[p.getX()-1][p.getY()-1]=1;}
				if(x>0){		  		    arraySegundaPasada[p.getX()-1][p.getY()]=1;}
				if(x>0 && y<height-1 ){     arraySegundaPasada[p.getX()-1][p.getY()+1]=1;}
				if( y>0 ){				    arraySegundaPasada[p.getX()][p.getY()-1]=1;}
				if(true ){				    arraySegundaPasada[p.getX()][p.getY()]=1;}
				if(y<height-1){      		arraySegundaPasada[p.getX()][p.getY()+1]=1;}
				if(x<width-1 && y>0 ){      arraySegundaPasada[p.getX()+1][p.getY()-1]=1;}
				if(x<width-1 ){ 		   	arraySegundaPasada[p.getX()+1][p.getY()]=1;}
				if(x<width-1 && y<height-1){arraySegundaPasada[p.getX()+1][p.getY()+1]=1;}
				
				if(x>1 && y>1 ){arraySegundaPasada[p.getX()-2][p.getY()-2]=1;}
				if(x>1 && y>0 ){arraySegundaPasada[p.getX()-2][p.getY()-1]=1;}
				if(x>1 ){arraySegundaPasada[p.getX()-2][p.getY()]=1;}
				if(x>1 && y<height-1 ){arraySegundaPasada[p.getX()-2][p.getY()+1]=1;}
				if(x>1 && y<height-2 ){arraySegundaPasada[p.getX()-2][p.getY()+2]=1;}
				if(x>0 && y>1 ){arraySegundaPasada[p.getX()-1][p.getY()-2]=1;}
				if(x>1 && y<height-2  ){arraySegundaPasada[p.getX()-1][p.getY()+2]=1;}
				if(y>1 ){arraySegundaPasada[p.getX()][p.getY()-2]=1;}
				if(y<height-2  ){arraySegundaPasada[p.getX()][p.getY()+2]=1;}
				if(x<width-1 && y>1 ){ arraySegundaPasada[p.getX()+1][p.getY()-2]=1;}
				if(x<width-1 && y<height-2 ){ arraySegundaPasada[p.getX()+1][p.getY()+2]=1;}
				if(x<width-2 && y>1 ){ arraySegundaPasada[p.getX()+2][p.getY()-2]=1;}
				if(x<width-2 && y>0 ){arraySegundaPasada[p.getX()+2][p.getY()-1]=1;}
				if(x<width-2){arraySegundaPasada[p.getX()+2][p.getY()]=1;}
				if(x<width-2 && y<height-1){arraySegundaPasada[p.getX()+2][p.getY()+1]=1;}
				if(x<width-2 && y<height-2){arraySegundaPasada[p.getX()+2][p.getY()+2]=1;}
				
				/*
				arraySegundaPasada[p.getX()-1][p.getY()-1]=1;
				arraySegundaPasada[p.getX()-1][p.getY()]=1;
				arraySegundaPasada[p.getX()-1][p.getY()+1]=1;
				arraySegundaPasada[p.getX()][p.getY()-1]=1;
				arraySegundaPasada[p.getX()][p.getY()]=1;
				arraySegundaPasada[p.getX()][p.getY()+1]=1;
				arraySegundaPasada[p.getX()+1][p.getY()-1]=1;
				arraySegundaPasada[p.getX()+1][p.getY()]=1;
				arraySegundaPasada[p.getX()+1][p.getY()+1]=1;
				
				arraySegundaPasada[p.getX()-2][p.getY()-2]=1;
				arraySegundaPasada[p.getX()-2][p.getY()-1]=1;
				arraySegundaPasada[p.getX()-2][p.getY()]=1;
				arraySegundaPasada[p.getX()-2][p.getY()+1]=1;
				arraySegundaPasada[p.getX()-2][p.getY()+2]=1;
				arraySegundaPasada[p.getX()-1][p.getY()-2]=1;
				arraySegundaPasada[p.getX()-1][p.getY()+2]=1;
				arraySegundaPasada[p.getX()][p.getY()-2]=1;
				arraySegundaPasada[p.getX()][p.getY()+2]=1;
				arraySegundaPasada[p.getX()+1][p.getY()-2]=1;
				arraySegundaPasada[p.getX()+1][p.getY()+2]=1;
				arraySegundaPasada[p.getX()+2][p.getY()-2]=1;
				arraySegundaPasada[p.getX()+2][p.getY()-1]=1;
				arraySegundaPasada[p.getX()+2][p.getY()]=1;
				arraySegundaPasada[p.getX()+2][p.getY()+1]=1;
				arraySegundaPasada[p.getX()+2][p.getY()+2]=1;
				*/
			}
		}
	 }
	
	
	public static void buscaCelulasSegundaPasada(){
		
		
		posX=0;
		posY=0;
		numX=0;
		numY=0;
		//LinkedList<Pixel> l= c.getlPixels();
		for(int x=1;x<width-1;x++){
			for(int y=1;y<height-1;y++){
				if(arraySegundaPasada[x][y]==0){
					color+=20;
					Celula c= new Celula(0,0,0,0,false);
					arraySegundaPasada[x][y]=1;//lo pongo negro
					posX+=x;
					posY+=y;
					numX++;
					numY++;
					c.getlPixels().add(new Pixel(x,y,color));
					buscaCerosAdyacentesSegundaPasada(x,y,c);
					c.setCentroX(posX/numX);
					c.setCentroY(posY/numY);
					cels.add(c);
				}
			}
		}
	}
	
	public static void buscaCerosAdyacentesSegundaPasada(int x, int y, Celula c){
		if(y>0 && x>0 && arraySegundaPasada[x-1][y-1]==0 ){posX+=x-1; posY+=y-1; arraySegundaPasada[x-1][y-1]=1; c.getlPixels().add(new Pixel(x-1,y-1,color)); buscaCerosAdyacentesSegundaPasada(x-1,y-1,c);}
		if(x>0 && arraySegundaPasada[x-1][y]==0){posX+=x-1; posY+=y; arraySegundaPasada[x-1][y]=1; c.getlPixels().add(new Pixel(x-1,y,color)); buscaCerosAdyacentesSegundaPasada(x-1,y,c);}
		if(y<height-1 && x>0 && arraySegundaPasada[x-1][y+1]==0 ){posX+=x-1; posY+=y+1; numY++; arraySegundaPasada[x-1][y+1]=1; c.getlPixels().add(new Pixel(x-1,y+1,color)); buscaCerosAdyacentesSegundaPasada(x-1,y+1,c);}
		if(y>0 && arraySegundaPasada[x][y-1]==0 ){posX+=x; posY+=y-1;arraySegundaPasada[x][y-1]=1; c.getlPixels().add(new Pixel(x,y-1,color)); buscaCerosAdyacentesSegundaPasada(x,y-1,c);}
		if(y<height-1 && arraySegundaPasada[x][y+1]==0 ){posX+=x; posY+=y+1; numY++; arraySegundaPasada[x][y+1]=1; c.getlPixels().add(new Pixel(x,y+1,color)); buscaCerosAdyacentesSegundaPasada(x,y+1,c);}
		if( y>0 && x<width-1 && arraySegundaPasada[x+1][y-1]==0 ){posX+=x+1; posY+=y-1; numX++;arraySegundaPasada[x+1][y-1]=1; c.getlPixels().add(new Pixel(x+1,y-1,color)); buscaCerosAdyacentesSegundaPasada(x+1,y-1,c);}
		if(x<width-1 && arraySegundaPasada[x+1][y]==0 ){posX+=x+1; posY+=y; numX++;arraySegundaPasada[x+1][y]=1; c.getlPixels().add(new Pixel(x+1,y,color)); buscaCerosAdyacentesSegundaPasada(x+1,y,c);}
		if(y<height-1 && x<width-1 && arraySegundaPasada[x+1][y+1]==0 ){posX+=x-1; posY+=y+1; numX++; numY++; arraySegundaPasada[x+1][y+1]=1; c.getlPixels().add(new Pixel(x+1,y+1,color)); buscaCerosAdyacentesSegundaPasada(x+1,y+1,c);}
	}
	
	public static int getIntFromColor(int Red, int Green, int Blue){
	    Red = (Red << 16) & 0x00FF0000; //Shift red 16-bits and mask out other stuff
	    Green = (Green << 8) & 0x0000FF00; //Shift Green 8-bits and mask out other stuff
	    Blue = Blue & 0x000000FF; //Mask out anything not blue.

	    return 0xFF000000 | Red | Green | Blue; //0xFF000000 for 100% Alpha. Bitwise OR everything together.
	}
	
	public static void borraCelulasPequenas(int tamanoMinimo) throws IOException {
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			if(c.getlPixels().size()<tamanoMinimo){
				cels.remove(i);
				i--;
			}
		}
	 }
	
	public static void pintaCelulasToBMPImage(String BMPFileName) throws IOException {
	    //BufferedImage image = ImageIO.read(getClass().getResource(BMPFileName));
		int color= -16755216;
		BufferedImage image = ImageIO.read(new File(BMPFileName));
		for(int i=0;i<cels.size();i++){
			Celula c= cels.get(i);
			//color = getIntFromColor((int)Math.random()*250,(int)Math.random()*250,(int)Math.random()*250);
			//color = getIntFromColor((int)Math.random()*250,(int)Math.random()*250,(int)Math.random()*250);
			color = getIntFromColor((int)Math.floor(Math.random()*255+1),(int)Math.floor(Math.random()*255+1),(int)Math.floor(Math.random()*255+1));
			//color = getIntFromColor(0,0,0);
			//System.out.println(color);
			for(int j=0;j<c.getlPixels().size();j++){
				Pixel p=c.getlPixels().get(j);
				//image.setRGB(p.getX(), p.getY(), color );
               
				/*image.setRGB(p.getX()-1, p.getY()-1, color );
				image.setRGB(p.getX()-1, p.getY(), color );
				image.setRGB(p.getX()-1, p.getY()+1, color );
				image.setRGB(p.getX(), p.getY()-1, color );
				image.setRGB(p.getX(), p.getY(), color );
				image.setRGB(p.getX(), p.getY()+1, color );
				image.setRGB(p.getX()+1, p.getY()-1, color );
				image.setRGB(p.getX()+1, p.getY(), color );
				image.setRGB(p.getX()+1, p.getY()+1, color );*/

				int x=p.getX();
				int y=p.getY();
				if (y>0 && x>0 && image.getRGB(p.getX()-1, p.getY()-1)!=Color.BLACK.getRGB())image.setRGB(p.getX()-1, p.getY()-1, color );
				if (x>0 &&image.getRGB(p.getX()-1, p.getY())!=Color.BLACK.getRGB())image.setRGB(p.getX()-1, p.getY(), color );
				if (y<height-1 && x>0 &&image.getRGB(p.getX()-1, p.getY()+1)!=Color.BLACK.getRGB())image.setRGB(p.getX()-1, p.getY()+1, color );
				if (y>0 && image.getRGB(p.getX(), p.getY()-1)!=Color.BLACK.getRGB())image.setRGB(p.getX(), p.getY()-1, color );
				if (image.getRGB(p.getX(), p.getY())!=Color.BLACK.getRGB())image.setRGB(p.getX(), p.getY(), color );
				if (y<height-1 && image.getRGB(p.getX(), p.getY()+1)!=Color.BLACK.getRGB())image.setRGB(p.getX(), p.getY()+1, color );
				if ( y>0 && x<width-1 &&image.getRGB(p.getX()+1, p.getY()-1)!=Color.BLACK.getRGB())image.setRGB(p.getX()+1, p.getY()-1, color );
				if (x<width-1 && image.getRGB(p.getX()+1, p.getY())!=Color.BLACK.getRGB())image.setRGB(p.getX()+1, p.getY(), color );
				if (y<height-1 && x<width-1 &&image.getRGB(p.getX()+1, p.getY()+1)!=Color.BLACK.getRGB())image.setRGB(p.getX()+1, p.getY()+1, color );
			}
			//image.setRGB(c.getCentroX(), c.getCentroY(), getIntFromColor(255,0,0));
			//color+= 1;
		}
	    File outputfile = new File("../saved.png");
	    ImageIO.write(image, "png", outputfile);
	 }
	
	
	static int posX=0;
	static int posY=0;
	static int numX=0;
	static int numY=0;
	static int color=1;
	
	public static void buscaCelulas(){
		
		
		posX=0;
		posY=0;
		numX=0;
		numY=0;
		//LinkedList<Pixel> l= c.getlPixels();
		for(int x=1;x<width-1;x++){
			for(int y=1;y<height-1;y++){
				if(arrayConBordes[x][y]==0){
					color+=20;
					Celula c= new Celula(0,0,0,0,false);
					arrayConBordes[x][y]=1;//lo pongo negro
					posX+=x;
					posY+=y;
					numX++;
					numY++;
					c.getlPixels().add(new Pixel(x,y,color));
					buscaCerosAdyacentes(x,y,c);
					c.setCentroX(posX/numX);
					c.setCentroY(posY/numY);
					cels.add(c);
				}
			}
		}
	}
	
	
	public static void buscaCerosAdyacentes(int x, int y, Celula c){
		if(x>0 && y>0&& arrayConBordes[x-1][y-1]==0){posX+=x-1; posY+=y-1; arrayConBordes[x-1][y-1]=1; c.getlPixels().add(new Pixel(x-1,y-1,color)); buscaCerosAdyacentes(x-1,y-1,c);}
		if(x>0 && arrayConBordes[x-1][y]==0){posX+=x-1; posY+=y; arrayConBordes[x-1][y]=1; c.getlPixels().add(new Pixel(x-1,y,color)); buscaCerosAdyacentes(x-1,y,c);}
		if(x>0 && y<height-1 && arrayConBordes[x-1][y+1]==0){posX+=x-1; posY+=y+1; numY++; arrayConBordes[x-1][y+1]=1; c.getlPixels().add(new Pixel(x-1,y+1,color)); buscaCerosAdyacentes(x-1,y+1,c);}
		if(y>0 && arrayConBordes[x][y-1]==0){posX+=x; posY+=y-1;arrayConBordes[x][y-1]=1; c.getlPixels().add(new Pixel(x,y-1,color)); buscaCerosAdyacentes(x,y-1,c);}
		if(y<height-1 && arrayConBordes[x][y+1]==0){posX+=x; posY+=y+1; numY++; arrayConBordes[x][y+1]=1; c.getlPixels().add(new Pixel(x,y+1,color)); buscaCerosAdyacentes(x,y+1,c);}
		if(x<width-1 && y>0 && arrayConBordes[x+1][y-1]==0){posX+=x+1; posY+=y-1; numX++;arrayConBordes[x+1][y-1]=1; c.getlPixels().add(new Pixel(x+1,y-1,color)); buscaCerosAdyacentes(x+1,y-1,c);}
		if(x<width-1 && arrayConBordes[x+1][y]==0){posX+=x+1; posY+=y; numX++;arrayConBordes[x+1][y]=1; c.getlPixels().add(new Pixel(x+1,y,color)); buscaCerosAdyacentes(x+1,y,c);}
		if(x<width-1 && y<height-1 && arrayConBordes[x+1][y+1]==0){posX+=x-1; posY+=y+1; numX++; numY++; arrayConBordes[x+1][y+1]=1; c.getlPixels().add(new Pixel(x+1,y+1,color)); buscaCerosAdyacentes(x+1,y+1,c);}
	}
	
	
	
	public static void buscaBordes(){
		arrayConBordes = new int[width][height];
		for(int x=1;x<width-1;x++){
			for(int y=1;y<height-1;y++){
				if(array2D[x][y]==0){
					/*if((x+1)==width){
						arrayConBordes[x][y]=array2D[x][y]+array2D[x-1][y-1]+array2D[x-1][y]+
								array2D[x-1][y+1]+array2D[x][y-1];
								
					}else{
						if((y+1)==height){
							arrayConBordes[x][y]=array2D[x][y]+array2D[x-1][y-1]+array2D[x-1][y]+
									array2D[x-1][y+1]+array2D[x][y-1]+array2D[x+1][y-1]+
									array2D[x+1][y-1]+array2D[x+1][y]+array2D[x+1][y+1];
						}else{*/
					//System.out.println("x="+x+"y=" +y);
							arrayConBordes[x][y]=array2D[x-1][y-1]+array2D[x-1][y]+
									array2D[x-1][y+1]+array2D[x][y-1]+array2D[x][y]+array2D[x][y+1]+
									array2D[x+1][y-1]+array2D[x+1][y]+array2D[x+1][y+1];
						//}
					//}
					
				}else{
					arrayConBordes[x][y]=9;
				}
			}
		}
		
	}
	
	
	public static void buscaBordesSegunda(){
		//arrayConBordes = new int[width][height];
		for(int x=1;x<width-1;x++){
			for(int y=1;y<height-1;y++){
				if(arrayConBordes[x][y]==0){
					/*if((x+1)==width){
						arrayConBordes[x][y]=array2D[x][y]+array2D[x-1][y-1]+array2D[x-1][y]+
								array2D[x-1][y+1]+array2D[x][y-1];
								
					}else{
						if((y+1)==height){
							arrayConBordes[x][y]=array2D[x][y]+array2D[x-1][y-1]+array2D[x-1][y]+
									array2D[x-1][y+1]+array2D[x][y-1]+array2D[x+1][y-1]+
									array2D[x+1][y-1]+array2D[x+1][y]+array2D[x+1][y+1];
						}else{*/
					//System.out.println("x="+x+"y=" +y);
							arrayConBordes[x][y]=arrayConBordes[x-1][y-1]+arrayConBordes[x-1][y]+
							arrayConBordes[x-1][y+1]+arrayConBordes[x][y-1]+arrayConBordes[x][y]+arrayConBordes[x][y+1]+
							arrayConBordes[x+1][y-1]+arrayConBordes[x+1][y]+arrayConBordes[x+1][y+1];
						//}
					//}
					
				}else{
					arrayConBordes[x][y]=9;
				}
			}
		}
		
	}
	
	
	public static void pinta() throws IOException{
		FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("../prueba2.txt");
        pw = new PrintWriter(fichero);
        for(int x=0;x<width;x++){
        	pw.println();
			for(int y=0;y<height;y++){
				pw.print(arrayConBordes[x][y]);
			}
        }
        pw.close();
	}
	
	public static void pintaFinal() throws IOException{
		FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("../prueba3.txt");
        pw = new PrintWriter(fichero);
        for(int x=0;x<width;x++){
        	pw.println();
			for(int y=0;y<height;y++){
				pw.print(arrayConBordes[x][y]);
			}
        }
        pw.close();
	}
	
	public static void pintaFinal2() throws IOException{
		FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("../prueba4.txt");
        pw = new PrintWriter(fichero);
        for(int x=0;x<width;x++){
        	pw.println();
			for(int y=0;y<height;y++){
				if(arrayConBordes[x][y]==0){
					pw.print(0);
				}else{
					pw.print(1);
				}
			}
        }
        pw.close();
	}
	
	public static void seeBMPImage(String BMPFileName) throws IOException {
	    //BufferedImage image = ImageIO.read(getClass().getResource(BMPFileName));
		BufferedImage image = ImageIO.read(new File(BMPFileName));
		height= image.getHeight();
		width=	image.getWidth();
	   // int[][] array2D = new int[image.getWidth()][image.getHeight()];
	    array2D = new int[image.getWidth()][image.getHeight()];

	    FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("../prueba.txt");
        pw = new PrintWriter(fichero);
  
	    
	    for (int xPixel = 0; xPixel < image.getWidth(); xPixel++)
	        {
	    	//System.out.println();
	    	pw.println(" ");
	            for (int yPixel = 0; yPixel < image.getHeight(); yPixel++)
	            {
	                int color = image.getRGB(xPixel, yPixel);
	                
	                if (color==Color.BLACK.getRGB()) {
	                    array2D[xPixel][yPixel] = 1;
	                    pw.print("1");
	                    //System.out.print(1);
	                } else {
	                    array2D[xPixel][yPixel] = 0; // ?
	                    pw.print("0");
	                    //System.out.print(0);
	                }
	                /*if (color==Color.WHITE.getRGB()) {
	                    array2D[xPixel][yPixel] = 0;
	                    pw.print("0");
	                    //System.out.print(1);
	                } else {
	                    array2D[xPixel][yPixel] = 1; // ?
	                    pw.print("1");
	                    //System.out.print(0);
	                }*/
	            }
	        }
	    pw.close();
	 }
	
	public static void extractColors (String ImageName) throws IOException {
		BufferedImage img = ImageIO.read(new File(ImageName));
		int h=img.getHeight();
		int w=img.getWidth();
		// you should stop here
		//byte[][] green = new byte[30][40];
		System.out.println("colores" +h+" "+w);
		for(int x=0; x<w; x++){
		  for(int y=0; y<h; y++){
		     int color = img.getRGB(x,y);
		     System.out.println((byte)(color>>24)+","+(byte)(color>>16)+","+(byte)(color>>8)+","+(byte)(color) +"-");
		     //alpha[x][y] = (byte)(color>>24);
		     //red[x][y] = (byte)(color>>16);
		     //green[x][y] = (byte)(color>>8);
		     //blue[x][y] = (byte)(color);
		  }
		}
	//	byte[][] inputData = green;
	}
	
	
	public static byte[] extractBytes (String ImageName) throws IOException {
		 // open image
		 File imgPath = new File(ImageName);
		 BufferedImage bufferedImage = ImageIO.read(imgPath);

		 // get DataBufferBytes from Raster
		 WritableRaster raster = bufferedImage .getRaster();
		 DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();

		 return ( data.getData() );
		}
}
