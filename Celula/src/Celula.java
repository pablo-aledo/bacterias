import java.util.LinkedList;

public class Celula {

	int longitud;
	int centroX;
	int centroY;
	int ancho;
	boolean fijada;
	LinkedList <Pixel> lPixels;
	public Celula(int longitud, int centroX,int centroY, int ancho, boolean fijada) {
		super();
		this.longitud = longitud;
		this.centroX = centroX;
		this.centroY = centroY;
		this.ancho = ancho;
		this.fijada = fijada;
		this.lPixels=new LinkedList<Pixel>();
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public int getCentroX() {
		return centroX;
	}

	public void setCentroX(int centroX) {
		this.centroX = centroX;
	}

	public int getCentroY() {
		return centroY;
	}

	public void setCentroY(int centroY) {
		this.centroY = centroY;
	}
	
	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public boolean isFijada() {
		return fijada;
	}

	public void setFijada(boolean fijada) {
		this.fijada = fijada;
	}

	public LinkedList<Pixel> getlPixels() {
		return lPixels;
	}

	public void setlPixels(LinkedList<Pixel> lPixels) {
		this.lPixels = lPixels;
	}
	

}
